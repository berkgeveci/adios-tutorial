from adios2.file_reader import FileReader
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# with FileReader("block.bp", comm=comm) as r:
#    temperature = r.read("temperature")
# print(temperature.shape)
r = FileReader("block.bp", comm=comm)