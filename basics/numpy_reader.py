from adios2.stream import Stream
from adios2.file_reader import FileReader
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
nx = 1024
nxlocal = nx // size
shape = [size * nxlocal]
start = [rank * nxlocal]
count = [nxlocal]

with Stream("first.bp", "r", comm=comm) as s:
    for i, step in enumerate(s.steps()):
        if i == 5:
            break
    temperature = s.read("temperature", start=start, count=count)
#    print(temperature)

#with FileReader("first.bp", comm=comm) as r:
#    print(dir(r.variables()))
#    temperature = r.read("temperature", start=start, count=count)