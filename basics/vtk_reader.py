from adios2.stream import Stream
from adios2.file_reader import FileReader
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

with FileReader("vtk.bp") as r:
   read_array = r.read("RTData")
