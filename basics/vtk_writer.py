from vtkmodules.vtkImagingCore import vtkRTAnalyticSource
from vtkmodules.numpy_interface import dataset_adapter as dsa
from mpi4py import MPI
from adios2.stream import Stream
from adios2.file_reader import FileReader
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

nx = 10
ny = nx
nz = 20

source = vtkRTAnalyticSource()
source.SetWholeExtent(0, nx-1, 0, ny-1, 0, nz-1)
source.UpdatePiece(rank, size, 0)
image_data = dsa.WrapDataObject(source.GetOutput())
array = image_data.PointData['RTData']
array = array.reshape(image_data.GetDimensions()[::-1])
start = image_data.GetExtent()[0:5:2]
start = start[::-1]
shape = np.array(source.GetWholeExtent()[1:6:2]) - np.array(source.GetWholeExtent()[0:5:2]) + 1
shape = tuple(shape[::-1])

with Stream("vtk.bp", "w", comm=comm) as s:
    s.write("RTData", content=array, shape=shape, start=start, count=array.shape)

with FileReader("vtk.bp", comm=comm) as r:
    read_array = r.read("RTData", start=start, count=array.shape)

assert (read_array == array).all()
