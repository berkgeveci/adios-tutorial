from adios2.stream import Stream
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
nx = 1024
nxlocal = nx // size
NSteps = 10
shape = [size * nxlocal]
start = [rank * nxlocal]
count = [nxlocal]

temperature = np.arange(0.0, 1.0, 1/nx, dtype=np.float32)

with Stream("first.bp", "w", comm=comm) as s:
    for _ in s.steps(10):
        if s.current_step() == 0:
            s.write("size", np.array([nx]))
        temperature *= 2
        s.write("temperature", content=temperature[rank*nxlocal:(rank+1)*nxlocal],
                 shape=shape, start=start, count=count)
