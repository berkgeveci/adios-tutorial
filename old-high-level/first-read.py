from mpi4py import MPI
import numpy as np
import adios2

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
nx = 1024
nxlocal = nx // size
shape = [size * nxlocal]
start = [rank * nxlocal]
count = [nxlocal]

with adios2.open("first.bp", "r", comm) as fh:
    for fstep in fh:

        # inspect variables in current step
        step_vars = fstep.available_variables()

        # print variables information
        if rank == 0:
            for name, info in step_vars.items():
                print("variable_name: " + name)
                for key, value in info.items():
                    print("\t" + key + ": " + value)

        # track current step
        step = fstep.current_step()
        if( step == 0 ):
            size_in = fstep.read("size")

        # read variables return a numpy array with corresponding selection
        temperature = fstep.read("temperature", start, count)
        if rank == 1:
            print(temperature)
