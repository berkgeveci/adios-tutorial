from mpi4py import MPI
import numpy as np
import adios2

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
nx = 1024
nxlocal = nx // size
NSteps = 10
shape = [size * nxlocal]
start = [rank * nxlocal]
count = [nxlocal]

temperature = np.arange(0.0, 1.0, 1/nx, dtype=np.float32)

with adios2.open("first.bp", "w", comm, "BP5") as fh:
    fh.set_parameter("NumSubFiles", "2")

    for i in range(0, NSteps):
        temperature *= 2

        if(rank == 0 and i == 0):
            fh.write("size", np.array([size]))

        # advances to next step
        fh.write("temperature", temperature[rank*nxlocal:(rank+1)*nxlocal],
                 shape, start, count, end_step=True)
