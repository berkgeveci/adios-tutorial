#from mpi4py import MPI
import numpy as np
import adios2

#comm = MPI.COMM_WORLD
#rank = comm.Get_rank()

# with adios2.open("second.bp", "r") as fh:
#     for fstep in fh:

#         # inspect variables in current step
#         step_vars = fstep.available_variables()

#         # print variables information
#         for name, info in step_vars.items():
#             print("variable_name: " + name)
#             for key, value in info.items():
#                 print("\t" + key + ": " + value)

#         # read variables return a numpy array with corresponding selection
#         temperature = fstep.read("temperature", 1)
# #        print(temperature)

with adios2.open("second.bp", "rra") as fh:
    temperature = fh.read("temperature", [], [], 0, 1, 0)
    print(temperature)

# adios = adios2.ADIOS()
# ioReader = adios.DeclareIO("reader")
# reader = ioReader.Open("second.bp", adios2.Mode.ReadRandomAccess)
# temp_var = ioReader.InquireVariable("temperature")
# temp_var.SetStepSelection([0,1])
# temp_var.SetBlockSelection(0)
# print(temp_var.Count())
# temperature = np.empty(temp_var.Count(), dtype=np.float64)
# reader.Get(temp_var, temperature)
# reader.PerformGets()
# print(temperature)

