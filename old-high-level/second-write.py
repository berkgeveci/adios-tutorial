from mpi4py import MPI
import numpy as np
import adios2
import random

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
NSteps = 10

nx = random.randint(100, 150)

with adios2.open("second.bp", "w", comm, "BP5") as fh:
    fh.set_parameter("NumSubFiles", "2")

    # NSteps from application
    for i in range(0, NSteps):
        temperature = np.random.rand(nx) * (rank+1)
        # advances to next step
        fh.write("temperature", temperature, [], [], [nx], end_step=True)
